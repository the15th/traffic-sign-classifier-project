# **Traffic Sign Recognition** 


The goals / steps of this project are the following:
* Load the data set (see below for links to the project data set)
* Explore, summarize and visualize the data set
* Design, train and test a model architecture
* Use the model to make predictions on new images
* Analyze the softmax probabilities of the new images
* Summarize the results with a written report

#### Data Set Summary & Exploration
I used the numpy library to calculate summary statistics of the traffic
signs data set:
* Number of training examples = 34799
* Number of testing examples = 12630
* Image data shape = (32, 32, 3)
* 43 classes/label in the dataset

[//]: # (Image References)

[image1]: ./writeup-images/histogram.jpg "Visualization"
[image2]: ./writeup-images/sample-images.jpg "Sample images" 
[image3]: ./writeup-images/after-gray-and-norm.jpg "Grayscaling and Normalization"
[image4]: ./traffic-signs-images/12-priority-road.jpg "Priority Road"
[image5]: ./traffic-signs-images/14-stop.jpg "Stop"
[image6]: ./traffic-signs-images/18-generic-caution.jpg "Generic caution"
[image7]: ./traffic-signs-images/22-bumpy-road.jpg "Bumpy Road"
[image8]: ./traffic-signs-images/26-traffic-signals.jpg "Traffic Signal"


Here is a link to my [project code](https://github.com/tremalnaik/carnd-traffic-sign-classifier-project-carnd-traffic-sign/blob/master/Traffic_Sign_Classifier.ipynb)


Here is an exploratory visualization of the data set.

![alt text][image1]

It is a bar chart showing samples data for each class of the train dataset and you can see at first sight how the data samples are unbalanced, some class have more than 2000 samples while others have less than 250, usually it is good and common practice to add samples to classes lacking data 

##### Sample Images
![alt text][image2]


### Design and Test a Model Architecture

#### Preprocessing
Is used a pickled dataset from the course repo, with images already resized to 32x32 pixel and each training, validation and test set splitted in their respective folders. I have 34799 samples data in training-set, 12630 in test-set and 4410 validation samples. The images in all the dataset are converted to grayscale and normalized using its mean and standard deviation.
Here is an example of a traffic sign image after grayscaling and normalization. 
![alt text][image3]

Normalization is a necessary step to make the dataset more robust and not ill-conditioned, i.e. shrinking and rescaling all the input samples in a predefinite range(like 0->1  or -1->1 interval for example) guarantee a more stable convergence of weight and biases, limit the excursion of a set of values within a certain default range. 
Converting from color-to-grayscale has little or marginal impact on the accuracy on this project, but i made this convertion only let work the algorithms on one single channel instead of the tree RGB.

### Model Architecture

| Layer         		|     Description	        					| 
|:---------------------:|:---------------------------------------------:| 
| Input         		| 32x32x1 Grayscale and normalized images   	|
| Convolution 1     	| 1x1 stride, same padding, outputs 28x28x6 	|
| Activation 1			| RELU											|
| Pooling layer 1	   	| 2x2 stride, same padding, outputs 14x14x6 	|
| Convolution 2     	| 1x1 stride, same padding, outputs 10x10x16 	|
| Activation 2			| RELU											|
| Pooling layer 2	   	| 2x2 stride, same padding, outputs 5x5x16 		|
| Flatten layer 1	    | output 400                            		|
| Fully connected 1		| outputs 120   								|
| Activation 3 			| RELU											|
| Dropout 1 			| keep probability 0.6							|
| Fully connected 2		| outputs 84   									|
| Activation 4			| RELU											|
| Dropout 2 			| keep probability 0.6							|
| Fully connected 3		| outputs 43        							|
| Softmax				|            									|


#### Model Training
The model was trained using Adam optimizer, batch size of 128 samples, over 50 epochs with a learning rate of 0,001.

#### Solution Approach
As a first step, I started with LeNet-5 implementation on the well knowed MNIST dataset shown at the end of  CNN classroom, I changed the number of classes from 10 to 43(i.e. number classes in the traffic-sign dataset) and started the first training on the validation set over 10 epochs, gaining about a 89% validation accuracy as expected. 

Then I decided to convert the images to grayscale and normalized the input (train, test, and validation dataset) using its mean and standard deviation the to increase the validation accuracy, that rised-up to 92% over 30 epochs.


Then I tried adding two dropout layers at the end of the first and the second fully connected layer, to prevent overfitting and optimize the performance, each with a keep-probability of 0.6. After that step the validation accuracy rised up to 96-97% over 50 epochs.

Consequently I started to test the test-set and I obtained a test-accuracy of 94%.

My final model results were:
* validation set accuracy of  96%
* test set accuracy of 94%

### Test a Model on New Images

#### Acquiring New Images
Here are five German traffic signs that I found on the web:

![alt text][image4] 
![alt text][image5] 
![alt text][image6] 
![alt text][image7] 
![alt text][image8]

The traffic signs in the images above are well defined without distortions or noise, the shapes and the edges are clearly visible with good light intensity. Under this conditions it is expected a clear identification from the newly-trained classifier that should match the test-set accuracy
#### Performance on New Images

Here are the results of the prediction:

| Image			        |     Prediction	        					| 
|:---------------------:|:---------------------------------------------:| 
| Bumpy Road			| Bumpy Road									|
| Stop signal			| Stop signal									|
| Traffic signals		| Traffic signals								|
| Priority road			| Priority road      							|
| General caution     	| General caution  								| 

The model was able to correctly guess 5 of the 5 traffic signs, which gives an accuracy of 100%. This compares favorably to the accuracy on the test set of 94%

#### Softmax Probabilities
For all images, the model is sure about sign (probability over 0.95). The top five soft max probabilities were

| Probability         	|     Prediction	        					| 
|:---------------------:|:---------------------------------------------:| 
| .99					| Bumpy Road									|
| 1	        			| Stop signal					 				|
| .99     				| Traffic signals 								|
| 1  				    | Priority road      							|
| 1         			| General caution   							| 


